package com.dinhman.controller;

import com.dinhman.entity.course.CourseEntity;
import com.dinhman.exception.ResourceNotFound;
import com.dinhman.service.course.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dinh Man on 05/03/2016.
 */
@Controller
public class IndexPage {

    @Autowired
    private CourseService courseService;

    @ModelAttribute(value = "allCourse")
    public List<CourseEntity> findAllCourse() {
        List<CourseEntity> listCourse = courseService.findAllCourse();
        return listCourse;
    }


    @ModelAttribute(value = "topCourses")
    public List<CourseEntity> findTopCourse() {
        List<CourseEntity> listCourse = new ArrayList<>();
        return listCourse;
    }

    @RequestMapping(value = {"/", "index"})
    public String index() {
        return "index";
    }
}
