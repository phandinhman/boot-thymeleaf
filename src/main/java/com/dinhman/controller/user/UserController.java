package com.dinhman.controller.user;

import com.dinhman.entity.user.UserEntity;
import com.dinhman.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

/**
 * Created by Dinh Man on 18/04/2016.
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String checkLogin(@ModelAttribute UserEntity userEntity, HttpSession session) {

        UserEntity anotherUser = userService.checkLogin(userEntity);

        System.out.println(anotherUser);

        if (anotherUser != null) {
            session.setAttribute("user", anotherUser);
            return "index";
        } else {
            return "index";
        }

    }

}
