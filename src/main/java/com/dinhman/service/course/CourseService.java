package com.dinhman.service.course;

import com.dinhman.entity.course.CourseEntity;
import com.dinhman.exception.ResourceNotFound;

import java.util.List;

/**
 * Created by Dinh Man on 18/03/2016.
 */
public interface CourseService {
    /**
     * Get all course from database.
     *
     * @return list course.
     */

    List<CourseEntity> findAllCourse();

    /**
     * Get one course by id.
     *
     * @param idCourse id of course.
     * @return course.
     */
    CourseEntity findById(long idCourse);


    /**
     * Update one course.
     *
     * @param courseEntity course need edit.
     * @return course updated.
     */
    CourseEntity update(CourseEntity courseEntity) throws ResourceNotFound;


    /**
     * Delete course.
     *
     * @param id id of course.
     * @return course deleted.
     */
    CourseEntity delete(long id) throws ResourceNotFound;

}
