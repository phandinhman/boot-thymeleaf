package com.dinhman.service.course;

import com.dinhman.entity.course.CourseEntity;
import com.dinhman.exception.ResourceNotFound;
import com.dinhman.repository.course.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Dinh Man on 18/03/2016.
 */
@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Override
    public List<CourseEntity> findAllCourse() {
        return courseRepository.findAll();
    }

    @Override
    public CourseEntity findById(long idCourse) {
        return courseRepository.findOne(idCourse);
    }

    @Override
    public CourseEntity update(CourseEntity courseEntity) throws ResourceNotFound {
        CourseEntity anotherCourse = courseRepository.findOne(courseEntity.getId());
        if (anotherCourse != null) {
            return courseRepository.save(anotherCourse);
        } else {
            throw new ResourceNotFound("Course not found.");
        }

    }

    @Override
    public CourseEntity delete(long id) throws ResourceNotFound {
        CourseEntity courseEntity = courseRepository.findOne(id);
        if (courseEntity != null) {
            courseRepository.delete(id);
            return courseEntity;
        } else {
            throw new ResourceNotFound("Ebook not found");
        }
    }


}
