package com.dinhman.service.user;

import com.dinhman.entity.user.UserEntity;

/**
 * Created by Dinh Man on 18/04/2016.
 */
public interface UserService {

    UserEntity checkLogin(UserEntity userEntity);

}
