package com.dinhman.service.user;

import com.dinhman.entity.user.UserEntity;
import com.dinhman.exception.ValidateException;
import com.dinhman.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Dinh Man on 18/04/2016.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserEntity checkLogin(UserEntity userEntity) {

        if (userEntity != null) {
            UserEntity anotherUser = userRepository.findByEmailAndPassword(userEntity.getEmail(), userEntity.getPassword());
            return anotherUser;
        } else {
            throw  new ValidateException("Invalid data.");
        }

    }
}
