package com.dinhman.service.book;

import com.dinhman.entity.book.BookEntity;
import com.dinhman.exception.ResourceNotFound;
import com.dinhman.repository.book.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.websocket.server.ServerEndpoint;
import java.util.List;

/**
 * Created by Dinh Man on 16/04/2016.
 */
@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public List<BookEntity> findAllBook() {
        return bookRepository.findAll();
    }

    @Override
    public BookEntity findById(long idEbook) {
        BookEntity bookEntity = bookRepository.findOne(idEbook);
        if (bookEntity != null) {
            return bookEntity;
        } else {
            throw new ResourceNotFound("Book not found");
        }
    }

    @Override
    public BookEntity update(BookEntity ebookEntity) throws ResourceNotFound {
        BookEntity anotherEbook = bookRepository.findOne(ebookEntity.getId());
        if (anotherEbook != null) {
            anotherEbook.setDetail(ebookEntity.getDetail());
            anotherEbook.setImage(ebookEntity.getImage());
            anotherEbook.setName(ebookEntity.getName());
            return bookRepository.save(anotherEbook);
        } else {
            throw new ResourceNotFound("Ebook not found");
        }
    }

    @Override
    public BookEntity delete(long id) throws ResourceNotFound {
        BookEntity bookEntity = bookRepository.findOne(id);
        if (bookEntity != null) {
            bookRepository.delete(id);
            return bookEntity;
        } else {
            throw new ResourceNotFound("Ebook not found");
        }
    }
}
