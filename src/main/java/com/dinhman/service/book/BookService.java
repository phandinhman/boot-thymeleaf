package com.dinhman.service.book;

import com.dinhman.entity.book.BookEntity;
import com.dinhman.exception.ResourceNotFound;

import java.util.List;

/**
 * Created by Dinh Man on 16/04/2016.
 */
public interface BookService {
    /**
     * Get all book from database.
     *
     * @return list book.
     */

    List<BookEntity> findAllBook();

    /**
     * Get one book by id.
     *
     * @param idBook id of book.
     * @return book.
     */
    BookEntity findById(long idBook);


    /**
     * Update one book.
     *
     * @param bookEntity book need edit.
     * @return book updated.
     */
    BookEntity update(BookEntity bookEntity) throws ResourceNotFound;


    /**
     * Delete book.
     *
     * @param id id of book.
     * @return book deleted.
     */
    BookEntity delete(long id) throws ResourceNotFound;
}
