package com.dinhman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SfThymeleafCourseApplication {

    public static void main(String[] args) {
        SpringApplication.run(SfThymeleafCourseApplication.class, args);
    }
}
