package com.dinhman.entity.course;


import com.dinhman.entity.discount.DiscountEntity;
import com.dinhman.entity.enroll.EnrollEntity;
import com.dinhman.entity.rate.RateEntity;
import com.dinhman.entity.section.SectionEntity;
import com.dinhman.entity.user.UserEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Dinh Man on 06/04/2016.
 */
@Entity
@Table(name = "course")
public class CourseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private long id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne
    private UserEntity userId;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "courseEntity")
    private DiscountEntity discountEntity;

    @OneToMany(mappedBy = "courseId")
    private List<SectionEntity> sectionEntityList;

    @OneToMany(mappedBy = "courseId")
    private List<EnrollEntity> enrollEntityList;

    public CourseEntity() {
    }

    public CourseEntity(long id) {
        this.id = id;
    }

    public CourseEntity(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public UserEntity getUserId() {
        return userId;
    }

    public void setUserId(UserEntity userId) {
        this.userId = userId;
    }

    public DiscountEntity getDiscountEntity() {
        return discountEntity;
    }

    public void setDiscountEntity(DiscountEntity discountEntity) {
        this.discountEntity = discountEntity;
    }

    @XmlTransient
    public List<SectionEntity> getSectionEntityList() {
        return sectionEntityList;
    }

    public void setSectionEntityList(List<SectionEntity> sectionEntityList) {
        this.sectionEntityList = sectionEntityList;
    }

    @XmlTransient
    public List<EnrollEntity> getEnrollEntityList() {
        return enrollEntityList;
    }

    public void setEnrollEntityList(List<EnrollEntity> enrollEntityList) {
        this.enrollEntityList = enrollEntityList;
    }


}