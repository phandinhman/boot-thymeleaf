package com.dinhman.entity.comment;

import com.dinhman.entity.lecture.LectureEntity;
import com.dinhman.entity.user.UserEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Dinh Man on 17/04/2016.
 */
@Entity
@Table(name = "comment")
public class CommentEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private long id;
    @Column(name = "create_date")
    private String createDate;
    @Column(name = "delete_date")
    private String deleteDate;
    @Column(name = "update_date")
    private String updateDate;
    @Column(name = "content")
    private String content;
    @JoinColumn(name = "lecture_id", referencedColumnName = "id")
    @ManyToOne
    private LectureEntity lectureId;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne
    private UserEntity userId;

    public CommentEntity() {
    }

    public CommentEntity(Integer id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(String deleteDate) {
        this.deleteDate = deleteDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LectureEntity getLectureId() {
        return lectureId;
    }

    public void setLectureId(LectureEntity lectureId) {
        this.lectureId = lectureId;
    }

    public UserEntity getUserId() {
        return userId;
    }

    public void setUserId(UserEntity userId) {
        this.userId = userId;
    }

}

