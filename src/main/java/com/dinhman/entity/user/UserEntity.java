package com.dinhman.entity.user;

import com.dinhman.entity.comment.CommentEntity;
import com.dinhman.entity.course.CourseEntity;
import com.dinhman.entity.enroll.EnrollEntity;
import com.dinhman.entity.rate.RateEntity;
import com.dinhman.entity.role.RoleEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Dinh Man on 19/03/2016.
 */
@Entity
@Table(name = "user")
public class UserEntity implements Serializable {
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private long id;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @OneToMany(mappedBy = "userId")
    private List<CourseEntity> courseEntityList;

    @OneToMany(mappedBy = "userId")
    private List<CommentEntity> commentEntityList;

    @OneToMany(mappedBy = "userId")
    private List<EnrollEntity> enrollEntityList;

    @JoinColumn(name = "role_id", referencedColumnName = "id")
    @ManyToOne
    private RoleEntity roleId;

    public UserEntity() {
    }

    public UserEntity(Integer id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @XmlTransient
    public List<CourseEntity> getCourseEntityList() {
        return courseEntityList;
    }

    public void setCourseEntityList(List<CourseEntity> courseEntityList) {
        this.courseEntityList = courseEntityList;
    }

    @XmlTransient
    public List<CommentEntity> getCommentEntityList() {
        return commentEntityList;
    }

    public void setCommentEntityList(List<CommentEntity> commentEntityList) {
        this.commentEntityList = commentEntityList;
    }

    @XmlTransient
    public List<EnrollEntity> getEnrollEntityList() {
        return enrollEntityList;
    }

    public void setEnrollEntityList(List<EnrollEntity> enrollEntityList) {
        this.enrollEntityList = enrollEntityList;
    }

    public RoleEntity getRoleId() {
        return roleId;
    }

    public void setRoleId(RoleEntity roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", createDate=" + createDate +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
