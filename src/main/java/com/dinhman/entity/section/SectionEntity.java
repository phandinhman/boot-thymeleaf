package com.dinhman.entity.section;

import com.dinhman.entity.course.CourseEntity;
import com.dinhman.entity.lecture.LectureEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Dinh Man on 17/04/2016.
 */
@Entity
@Table(name = "section")
public class SectionEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "date_create")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "date_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "date_delete")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDelete;
    @OneToMany(mappedBy = "sectionId")
    private List<LectureEntity> lectureEntityList;
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    @ManyToOne
    private CourseEntity courseId;

    public SectionEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Date getDateDelete() {
        return dateDelete;
    }

    public void setDateDelete(Date dateDelete) {
        this.dateDelete = dateDelete;
    }

    @XmlTransient
    public List<LectureEntity> getLectureEntityList() {
        return lectureEntityList;
    }

    public void setLectureEntityList(List<LectureEntity> lectureEntityList) {
        this.lectureEntityList = lectureEntityList;
    }

    public CourseEntity getCourseId() {
        return courseId;
    }

    public void setCourseId(CourseEntity courseId) {
        this.courseId = courseId;
    }

}

