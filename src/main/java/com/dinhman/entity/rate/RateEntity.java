package com.dinhman.entity.rate;

import com.dinhman.entity.course.CourseEntity;
import com.dinhman.entity.enroll.EnrollEntity;
import com.dinhman.entity.user.UserEntity;
import com.dinhman.entity.vote.VoteEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Dinh Man on 17/04/2016.
 */
@Entity
@Table(name = "rate")
public class RateEntity implements Serializable {
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private long id;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    
    @Column(name = "content")
    private String content;

    @JoinColumn(name = "id", referencedColumnName = "id")
    @OneToOne
    private EnrollEntity enrollId;

    @JoinColumn(name = "vote_id", referencedColumnName = "id")
    @ManyToOne
    private VoteEntity voteId;

    public RateEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public EnrollEntity getEnrollId() {
        return enrollId;
    }

    public void setEnrollId(EnrollEntity enrollId) {
        this.enrollId = enrollId;
    }
}
