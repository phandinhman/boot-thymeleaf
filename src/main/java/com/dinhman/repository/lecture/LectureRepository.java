package com.dinhman.repository.lecture;

import com.dinhman.entity.lecture.LectureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Dinh Man on 17/04/2016.
 */
@Repository
public interface LectureRepository extends JpaRepository<LectureEntity, Long>{
}
