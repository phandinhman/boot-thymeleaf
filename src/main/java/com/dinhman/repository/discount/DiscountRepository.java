package com.dinhman.repository.discount;

import com.dinhman.entity.discount.DiscountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Dinh Man on 17/04/2016.
 */
@Repository
public interface DiscountRepository extends JpaRepository<DiscountEntity, Long>{
}
