package com.dinhman.repository.course;

import com.dinhman.entity.course.CourseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dinh Man on 18/03/2016.
 */
@Repository
public interface CourseRepository extends JpaRepository<CourseEntity, Long> {
}
