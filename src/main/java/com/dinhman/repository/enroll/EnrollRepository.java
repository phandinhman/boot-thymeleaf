package com.dinhman.repository.enroll;

import com.dinhman.entity.enroll.EnrollEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Dinh Man on 17/04/2016.
 */
@Repository
public interface EnrollRepository extends JpaRepository<EnrollEntity, Long>{
}
