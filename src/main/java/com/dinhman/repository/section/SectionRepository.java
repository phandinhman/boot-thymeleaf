package com.dinhman.repository.section;

import com.dinhman.entity.section.SectionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Dinh Man on 17/04/2016.
 */
@Repository
public interface SectionRepository extends JpaRepository<SectionEntity, Long>{
}
