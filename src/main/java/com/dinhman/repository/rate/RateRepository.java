package com.dinhman.repository.rate;

import com.dinhman.entity.rate.RateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Dinh Man on 17/04/2016.
 */
@Repository
public interface RateRepository extends JpaRepository<RateEntity, Long>{
}
